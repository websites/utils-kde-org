<?php
 $site_root = "../";
 $page_title = "Contact";
 include "header.inc";
?>

<h2><a name="report">Report bugs</a></h2>
 <p>
  <img src="kde-icons/ox32-bug.png" align="right" hspace="20" />
  If you encounter bugs or would like to propose features, please use the
  <a href="https://bugs.kde.org/wizard.cgi">Bug Reporting Wizard</a> at
  <a href="https://bugs.kde.org/">KDE Bug Tracking System</a>. 
 </p>

 <p>For any issues such as broken links &amp; spelling/grammar errors relating
  to the <a href="index.php">utils.kde.org</a> website please
  contact the <a href="mailto:kossebau@kde.org">webmaster</a>.
 </p>

<h2><a name="discuss">Discuss with other users</a></h2>
 <p>
  <img src="kde-icons/ox32-users.png" align="right" hspace="20" />
  For discussions about the usage of KDE Utilities, subscribe to the
  <a href="http://mail.kde.org/mailman/listinfo/kde">general KDE mailinglist</a> for users,
  if your needs were not already covered by browsing it's <a href="http://lists.kde.org/?l=kde&amp;r=1&amp;w=2">online archive</a>.
 </p>

 <p>
  Alternatively use the web-based <a href="http://forum.kde.org/">KDE forum</a>.
 </p>

 <p>
  Or chat on one of the <a href="http://userbase.kde.org/IRC_Channels">KDE-related IRC channels</a>
  in the <a href="http://www.freenode.net/">freenode</a> IRC Network,
  e.g. channel <a href="irc://irc.kde.org/#kde">#kde</a>.
 </p>

<h2><a name="talk">Talk to developers</a></h2>
 <p>
  <img src="kde-icons/ox32-users.png" align="right" hspace="20" />
  For discussions around the development of KDE Utilities, subscribe to the
  <a href="http://mail.kde.org/mailman/listinfo/kde-utils-devel">KDE Utilities developer mailinglist</a>, and try it's
  <a href="http://lists.kde.org/?l=kde-utils-devel&amp;r=1&amp;w=2">online archive</a>.
 </p>

 <p>
  If you want to provide a patch, please file a review request on the <a href="http://reviewboard.kde.org/">KDE Review Board</a> for the group <b>kdeutils</b>.
 </p>

<?php
 include "footer.inc";
?>
