<?php
$this->setName ("KDE Utilities");

$section =& $this->appendSection("Inform");
$section->appendLink("KDE Utilities Home","");
$section->appendLink("KDE Home","http://www.kde.org/",false);
// $section->appendLink("News","news/index.php");
// $section->appendLink("FAQ","faq/index.php");


$section =& $this->appendSection("Projects");
$section->appendDir("Ark","projects/ark");
$section->appendDir("Filelight","projects/filelight");
$section->appendDir("KBackup","projects/kbackup");
$section->appendDir("KCalc","projects/kcalc");
$section->appendDir("KCharSelect","projects/kcharselect");
$section->appendDir("KDiskFree","projects/kdf");
$section->appendDir("KFloppy","projects/kfloppy");
$section->appendDir("KGpg","projects/kgpg");
$section->appendDir("KTimer","projects/ktimer");
$section->appendDir("KDE Wallet Manager","projects/kwalletmanager");
$section->appendDir("Sweeper","projects/sweeper");


$section =& $this->appendSection("Communicate");
$section->appendLink("Contact","contact.php");

$section =& $this->appendSection("Develop");
$section->appendLink("Getting involved","https://community.kde.org/Get_Involved",false);
$section->appendLink("Localization","http://l10n.kde.org/stats/gui/trunk-kde4/package/kdeutils/",false);
$section->appendLink("Community Wiki","https://community.kde.org/KDE_Utils",false);
$section->appendLink("Sources Listing","https://invent.kde.org/",false);
$section->appendLink("Legacy API Documentation","https://api.kde.org/legacy/4.14-api/kdeutils-apidocs/",false);
$section->appendLink("Code Check Results","http://ebn.kde.org/krazy/index.php?component=kde-4.x&amp;module=kdeutils",false);
$section->appendLink("Patch Review Requests","https://invent.kde.org/groups/utilities/-/merge_requests",false);

$section =& $this->appendSection("Download");
$section->appendLink("Download","https://www.kde.org/download/",false);
?>
