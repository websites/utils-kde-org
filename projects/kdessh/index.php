<?php
    $site_root = "../";
    $page_title = 'kdessh';

    include ( "header.inc" );

    include( "project.inc" );

    $appinfo = new AppInfo( "kdessh" );
    //   $appinfo->setIcon( "../../kde-icons/ox32-app-okteta.png", "32", "32" );
    $appinfo->setVersion( "4.1.0" );
    $appinfo->setCopyright( "2000", "Geert Jansen" );
    $appinfo->setLicense("gpl");

    $appinfo->addAuthor("Geert Jansen", "jansen@kde.org", "Old maintainer");

//     $appinfo->show();
?>

 <p>
  kdessh runs a program on a remote host.
 </p>


 <p>
<?php
    $latestRelease = new LatestRelease($project);
    $latestRelease->show();
?>
 </p>

 <p>
  While kdessh was always released as part of kdeutils, it had been broken in the port to the KDE 4 Platform.
  Without a maintainer and obviously without any users, who would have complained about this otherwise,
  kdessh has been <b>removed</b> from the kdeutils module for version <b>4.4</b> and later of the KDE SC (to <a href="http://websvn.kde.org/tags/unmaintained/4/kdessh">tags/unmaintained/4/kdessh</a>).
 </p>


 <?php
//   kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

