<?php
    // They are used to link to git tags. It's like a VCS browser. But not.
    $vers = upstreamVersionsSince("17.12");

    $vers = array_merge($vers, set_version_for_range(15, 4, 17, 8, '1.21'));

    $vers['14.12'] = '1.20';
    for ($i = 14; $i >= 11; $i--)
      $vers["4." . $i] = '1.20';

    $vers['4.10'] = '1.13';

    for ($i = 9; $i >= 6; $i--)
      $vers["4." . $i] = '1.' . ($i + 3);

    $project = new Project('filelight', 'Filelight', $vers);
?>
