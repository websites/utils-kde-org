<?php
    $site_root = "../";
    $page_title = 'Filelight - Contact';

    include( "header.inc" );

    include( "project.inc" );

    $contactOverview = new ContactOverview($project);
    $contactOverview->show();

    include( "footer.inc" );
?>
