<?php
    $site_root = "../";
    $page_title = 'Filelight';

    include ( "header.inc" );

    include( "project.inc" );
?>

 <p>
  Filelight allows you to quickly understand exactly where your diskspace is being used by graphically representing your file system as a set of concentric segmented-rings. You can use it to locate hotspots of disk usage and then manipulate those areas using a file manager.
 </p>

 <p>
<?php
    $latestRelease = new LatestRelease($project);
    $latestRelease->show();
?>
 </p>

 <h2>Linux</h2>
 <p>
  Filelight for Linux is available for installation in most Linux distributions.
  For more information check out the <a href="http://www.kde.org/download/">KDE Download page</a>.
 </p>

 <h2>Windows</h2>
 <p>
 <a href="https://www.microsoft.com/en-us/p/filelight/9pfxcd722m2c">
   Latest version</a> of Filelight for Windows.
 </p>

  <h2>Other Platforms</h2>
  <p>
    Check out the <a href="http://www.kde.org/download/">KDE Download page</a>.
  </p>


 <?php
//   kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
