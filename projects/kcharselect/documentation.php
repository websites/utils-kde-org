<?php
    $site_root = "../";
    $page_title = 'KCharSelect - Documentation';

    include( "header.inc" );

    include( "project.inc" );

    $documentationOverview = new DocumentationOverview($project);
    $documentationOverview->show();

    include( "footer.inc" );
?>
