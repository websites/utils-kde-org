<?php

class VersionedPathWith3dot5 extends VersionedPath
{
    var $path;
    var $pathFor3dot5;

    function VersionedPathWith3dot5($path,$pathFor3dot5)
    {
        $this->path = $path;
        $this->pathFor3dot5 = $pathFor3dot5;
    }

    function path($versionId)
    {
        if ($versionId=="3.5")
            return $this->pathFor3dot5;
        else
            return $this->path;
    }
}

    $vers = upstreamVersionsSince("19.08");

    $vers = array_merge($vers, set_version_for_range(16, 12, 19, 4, '1.13'));
    $vers = array_merge($vers, set_version_for_range(15, 4, 16, 8, '1.12'));

    $vers['14.12'] = '1.11';

    for ($i = 14; $i >= 10; $i--)
       $vers["4." . $i] = '1.11';

    for ($i = 9; $i >= 0; $i--)
       $vers["4." . $i] = '1.' . ($i + 2);

    $vers['3.5'] = '1.1';

    $project = new Project('kcharselect', 'KCharSelect', $vers);
    $project->setDocPoFileNameList(null);
    $project->setManualList(null);
    $project->setOtherSourcesList(
        array("Library (kcharselect*.* files)"=>new VersionedPathWith3dot5("kde/kdelibs/repository/show/kdeui/widgets","kde/kdelibs/repository/show/kdeui")));
    $project->setOtherAPIList(
        array("Library (KCharSelect)"=>new VersionedPath("kdelibs-apidocs/kdeui/html/classKCharSelect.html")));
?>
