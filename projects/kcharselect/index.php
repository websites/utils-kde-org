<?php
    $site_root = "../";
    $page_title = 'KCharSelect';

    include ( "header.inc" );

    include( "project.inc" );

    $appinfo = new AppInfo( "KCharSelect" );
    $appinfo->setIcon( "../../kde-icons/ox32-app-accessories-character-map.png", "32", "32" );
    $appinfo->setVersion( "1.2" );
    $appinfo->setCopyright( "?", "?" );
    $appinfo->setLicense("gpl");

    $appinfo->addAuthor("Constantin Berzan", "exit3219@gmail.com", "Maintainer");
    $appinfo->addAuthor("Reginald Stadlbauer","reggie@kde.org","Author");

    $appinfo->addContributor("Daniel Laidig", "d.laidig@gmx.de", "New GUI, Unicode information, and general improvements" );
    $appinfo->addContributor("Nadeem Hasan", "nhasan@kde.org", "GUI cleanup and fixes" );
    $appinfo->addContributor("Ryan Cumming", "bodnar42@phalynx.dhs.org", "GUI cleanup and fixes" );
    $appinfo->addContributor("Benjamin C. Meyer", "ben+kcharselect@meyerhome.net", "XMLUI conversion" );
    $appinfo->addContributor("Bryce Nesbitt", "", "RTL support" );

//     $appinfo->show();
?>

 <p>
  KCharSelect is a tool to select special characters from all installed fonts and copy them into the clipboard.
 </p>

 <p>
<?php
    $latestRelease = new LatestRelease($project);
    $latestRelease->show();
?>
 </p>

 <div style="text-align: center;">
  <img src="images/kcharselect-1.3-screenie.jpg" alt="KCharSelect Screenshot" />
 </div>

 <p>
  You can browse characters by their category or quickly find a certain character by searching for its name.
  KCharSelect displays various information about the selected character. This includes not only the Unicode
  character name, but also aliases, general notes and cross references to similar characters. For technical
  use, different representations of the character are shown. KCharSelect also contains
  <a href="http://en.wikipedia.org/wiki/Han_unification">Unihan</a> data for East Asian characters to display
  English definitions and different pronunciations.
 </p>


 <?php
//   kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

