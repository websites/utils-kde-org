<?php
    $site_root = "../";
    $page_title = 'KBackup';

    include ( "header.inc" );

    include( "project.inc" );
?>

 <p>
  A Backup program with an easy to use User Interface.
 </p>

 <p>
<?php
    $latestRelease = new LatestRelease($project);
    $latestRelease->show();
?>
 </p>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
