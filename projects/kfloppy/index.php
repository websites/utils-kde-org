<?php
    $site_root = "../";
    $page_title = 'KFloppy';

    include ( "header.inc" );

    include( "project.inc" );

    $appinfo = new AppInfo( "KFloppy" );
    $appinfo->setIcon( "../../kde-icons/hi32-app-kfloppy.png", "32", "32" );
    $appinfo->setVersion( "4.1.0" );
    $appinfo->setCopyright( "1997", "Bernd Johannes Wuebben" );
    $appinfo->setLicense("gpl");

    $appinfo->addAuthor("Bernd Johannes Wuebben", "wuebben@kde.org", "Author and former maintainer");
    $appinfo->addContributor("Chris Howells", "howells@kde.org", "User interface re-design");
    $appinfo->addContributor("Adriaan de Groot", "groot@kde.org", "Add BSD support");
    $appinfo->addContributor("Nicolas Goutte", "goutte@kde.org", "Make KFloppy work again for KDE 3.4");

//     $appinfo->show();
?>

 <p>
  KFloppy is a utility that provides a straightforward graphical means
  to format 3.5" and 5.25" floppy disks.
 </p>

 <p>
<?php
    $latestRelease = new LatestRelease($project);
    $latestRelease->show();
?>
 </p>

 <p>
  KFloppy depends on external programs and therefore currently works <b>only</b>
  with Linux and BSD. Depending if you are using KFloppy on Linux or on BSD,
  KFloppy has slightly different features.
 </p>

 <p>
  Make sure that your floppy disk is <b>not</b> mounted.
  KFloppy cannot format a mounted floppy disk.
 </p>


 <?php
//   kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

