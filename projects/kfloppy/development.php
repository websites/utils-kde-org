<?php
    $site_root = "../";
    $page_title = 'KFloppy - Development';

    include( "header.inc" );

    include( "project.inc" );

    $developmentOverview = new DevelopmentOverview($project);
//     $blogs = new BlogLinks();
//     $blogs->addBlogLink( "Joe", "http://joe.wordpress.com/tag/floppy" );
//     $developmentOverview->setBlogs( $blogs );
    $developmentOverview->show();

    include( "footer.inc" );
?>


