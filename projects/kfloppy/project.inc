<?php
    $vers = upstreamVersionsSince("19.08");

    $vers = array_merge($vers, set_version_for_range(16, 4, 19, 4, '5.0'));
    $vers = array_merge($vers, set_version_for_range(14, 12, 15, 12, '4.9'));

    for ($i = 14; $i >= 9; $i--)
      $vers["4." . $i] = '4.9';

    for ($i = 8; $i >= 0; $i--) {
      $v = '4.' . $i;
      $vers[$v] = $v;
    }
    $vers['3.5'] = '3.5';

    $project = new Project('kfloppy', 'KFloppy', $vers);
?>
