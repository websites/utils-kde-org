<?php
    $site_root = "../";
    $page_title = 'KDELirc - Development';

    include( "header.inc" );

    include( "project.inc" );

    $developmentOverview = new DevelopmentOverview($project);
    $developmentOverview->show();

    include( "footer.inc" );
?>


