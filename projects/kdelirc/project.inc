<?php
    $project = new Project("kdelirc","KDELirc",array("4.4"=>"4.4","4.3"=>"4.3","3.5"=>"3.5"));

    $irkickManual = new Manual("IRKick",        "kdelirc/irkick");
    $kcmManual =    new Manual("Control module","kdelirc/kcmlirc");
    $project->setManualList(new VersionedArrayOfArray(array($irkickManual,$kcmManual)));
    $project->setDocPoFileNameList(new VersionedArrayOfArray(array("kdelirc_irkick","kdelirc_kcmlirc")));
    $project->setGuiPoFileNameList(new VersionedArrayOfArray(array("kdelirc","kcmlirc","irkick")));
?>
