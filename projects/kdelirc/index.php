<?php
    $site_root = "../";
    $page_title = 'KDELirc';

    include ( "header.inc" );

    include( "project.inc" );
?>

 <p>
  KDELirc is a KDE frontend for the <a href="http://lirc.org/">Linux Infrared Remote Control system (LIRC)</a>.
 </p>

 <p>
<?php
    $latestRelease = new LatestRelease($project);
    $latestRelease->show();
?>
 </p>

  <p>
    <b>Starting with the release of the KDE Applications 4.5 KDELirc is now continued as
    <a href="http://utils.kde.org/projects/kremotecontrol">KRemoteControl</a>.</b>
  </p>

  <div style="text-align: center;">
    <img src="images/kdelirc-4.3-screenie.jpg" alt="Screenshot of IRKick and the KDELirc control module" />
  </div>

  <p>
  KDELirc consists of two parts:
  <ol>
    <li>
      A systemtray applet named IRKick which acts as a proxy between the LIRC system and D-Bus.
      It maps between remote actions and configured D-Bus requests to control and to launch programs.
    </li>
    <li>
      A control module which features:
      <ol>
        <li>Overview of
          <ul>
            <li>installed remote controls, with mapping to predefined configurations (if given)</li>
            <li>configured modes and actions for the remote controls</li>
            <li>available profile bindings for program controlling</li>
          </ul>
        </li>
        <li>Configuration of
          <ul>
            <li>application bindings to remote control buttons manually or by given profiles</li>
            <li>application bindings grouping for each remote into modes</li>
        </li>
      </ol>
    </li>
  </ol>
 </p>

 <?php
//   kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

