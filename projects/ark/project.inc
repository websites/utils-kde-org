<?php
    $vers = upstreamVersionsSince("15.12");
    $vers["15.04"] = "2.19";
    $vers["14.12"] = "2.19";

    for ($i = 0; $i < 5; $i++)
        $vers["4." . (14 - $i)] = "2.19";

    for ($i = 8; $i >= 2; $i--)
        $vers["4." . $i] = "2.1" . $i;

    $vers["4.1"] = "2.10";
    $vers["4.0"] = "2.9";
    $vers["3.5"] = "2.6";

    $project = new Project("ark", "Ark", $vers);
?>
