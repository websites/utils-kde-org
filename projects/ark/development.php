<?php
    $site_root = "../";
    $page_title = 'Ark - Development';

    include( "header.inc" );

    include( "project.inc" );

    $developmentOverview = new DevelopmentOverview($project);

    $blogs = new BlogLinks();
    $blogs->addBlogLink( "Harald Hvaal", "http://metelliuscode.wordpress.com/category/ark/" );
    $blogs->addBlogLink( "Raphael Kubo da Costa", "http://www.kdedevelopers.org/blog/7305" );
    $developmentOverview->setBlogs( $blogs );
    $docs = new DocLinks();
    $docs->addDocLink("TechBase information", "http://techbase.kde.org/Projects/Utils/Ark",
        "The developers' page in the KDE TechBase.");
    $developmentOverview->setDocs( $docs );

    $developmentOverview->show();

    include( "footer.inc" );
?>


