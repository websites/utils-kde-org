<?php
    $site_root = "../";
    $page_title = 'Ark';

    include ( "header.inc" );

    include( "project.inc" );

    $appinfo = new AppInfo( "Ark" );
    $appinfo->setIcon( "../../kde-icons/ox32-app-utilities-file-archiver.png", "32", "32" );
    $appinfo->setVersion( "2.15" );
    $appinfo->setCopyright( "1997-2010", "The Various Ark Developers" );
    $appinfo->setLicense("gpl");

    $appinfo->addAuthor( "Harald Hvaal",
                         "haraldhv@stud.ntnu.no",
                         "Maintainer" );
    $appinfo->addAuthor( "Raphael Kubo da Costa",
                         "kubito@gmail.com",
                         "Maintainer" );
    $appinfo->addAuthor( "Henrique Pinto",
                         "henrique.pinto@kdemail.net",
                         "Former maintainer" );
    $appinfo->addAuthor( "Helio Chissini de Castro", "helio@kde.org",
                         "Former maintainer" );
    $appinfo->addAuthor( "Georg Robbers", "Georg.Robbers@urz.uni-hd.de" );
    $appinfo->addAuthor( "Roberto Selbach Teixeira", "maragato@kde.org" );
    $appinfo->addAuthor( "Francois-Xavier Duranceau", "duranceau@kde.org" );
    $appinfo->addAuthor( "Emily Ezust (Corel Corporation)", "emilye@corel.com" );
    $appinfo->addAuthor( "Michael Jarrett (Corel Corporation)", "michaelj@corel.com" );
    $appinfo->addAuthor( "Robert Palmbos", "palm9744@kettering.edu" );

    $appinfo->addContributor( "Bryce Corkins", "dbryce@attglobal.net",
                              "Icons" );
    $appinfo->addContributor( "Liam Smit", "smitty@absamail.co.za",
                              "Ideas, help with the icons" );
    $appinfo->addContributor(  "Andrew Smith", "",
                               "bkisofs code" ); //, "http://littlesvr.ca/misc/contactandrew.php" );

//     $appinfo->show();
?>

 <p>
  Ark is a program for managing various archive formats within the
  KDE environment.
 </p>

 <p>
<?php
    $latestRelease = new LatestRelease($project);
    $latestRelease->show();
?>
 </p>

 <p>
  Archives can be viewed, extracted, created and modified from within Ark.
  The program can handle various formats such as <b>tar</b>, <b>gzip</b>,
  <b>bzip2</b>, <b>zip</b>, <b>rar</b> and
  <b>lha</b> (if appropriate command-line programs are
  installed). Ark can work closely with Konqueror in the KDE
  environment to handle archives, if you install the Konqueror Integration plugin
  available in the kdeaddons package.
 </p>


 <?php
//   kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

