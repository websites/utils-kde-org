<?php
    $vers = upstreamVersionsSince("19.08");

    $vers = array_merge($vers, set_version_for_range(18, 8, 19, 4, '1.12'));

    $vers["18.04"] = "1.11";
    $vers["17.12"] = "1.10";

    # no version bump betwen 4.9 and 17.08
    $vers = array_merge($vers, set_version_for_range(14, 12, 17, 8, '1.9'));
    for ($i = 14; $i >= 9; $i--) {
        $vers["4." . $i] = "1.9";
    }

    for ($i= 8; $i >= 0; $i--) {
        $vers["4." . $i] = "1." . $i;
    }

    $project = new Project("sweeper", "Sweeper", $vers);
    $project->setDocPoFileNameList(null);
    $project->setManualList(null);
?>
