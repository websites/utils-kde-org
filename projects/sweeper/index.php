<?php
    $site_root = "../";
    $page_title = 'Sweeper';

    include ( "header.inc" );

    include( "project.inc" );

    $appinfo = new AppInfo( "Sweeper" );
    $appinfo->setIcon( "../../kde-icons/ox32-actions-trash-empty.png", "32", "32" );
    $appinfo->setVersion( "1.0" );
    $appinfo->setCopyright( "2003", "Ralf Hoelzer, Brian S. Stephan" );
    $appinfo->setLicense("gpl");

    $appinfo->addAuthor("Ralf Hoelzer", "ralf@well.com", "Original author");
    $appinfo->addAuthor("Brian S. Stephan", "bssteph@irtonline.org", "Maintainer");
    $appinfo->addContributor("Benjamin Meyer", "ben+kdeprivacy@meyerhome.net", "Thumbnail Cache");

//     $appinfo->show();

?>

 <p>
  Sweeper helps to clean unwanted traces the user leaves on the system.
 </p>

 <p>
<?php
    $latestRelease = new LatestRelease($project);
    $latestRelease->show();
?>
 </p>
 <p>
<!-- Some more description -->
 </p>


 <?php
//   kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

