<?php
    $site_root = "../";
    $page_title = 'Sweeper - Development';

    include( "header.inc" );

    include( "project.inc" );

    $developmentOverview = new DevelopmentOverview($project);
//     $blogs = new BlogLinks();
//     $blogs->addBlogLink( "Joe", "http://joe.wordpress.com/tag/sweeper" );
//     $developmentOverview->setBlogs( $blogs );

    $developmentOverview->show();

    include( "footer.inc" );
?>


