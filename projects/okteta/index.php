<?php
    $site_root = "../";
    $page_title = 'Okteta';

    include( "header.inc" );

    include( "project.inc" );
?>

 <p>
  Okteta is a simple editor for the raw data of files. This type of
  program is also called hex editor or binary editor.
 </p>

 <p>
<?php
    $latestRelease = new LatestRelease($project);
    $latestRelease->show();
?>
 </p>

 <p>
   Starting with KDE SC 4.6, Okteta became part of the <strong>kdesdk</strong> module.
 </p>

 <div style="text-align: center;">
  <img src="images/okteta-0.3-screenie.jpg" alt="Okteta Screenshot" />
 </div>

 <p>
  The data is displayed in two variants:
  as the numeric values of the bytes and as the characters assigned to the values.
  Values and characters can be shown either in two columns (the traditional display
  in hex editors) or in rows with the value on top of the character.
  Editing can be done both for the values and for the characters.
  Besides the usual editing capabilities Okteta also brings a small set
  of tools, like a table listing decodings into common simple data types,
  a table listing all possible bytes with its' character and value
  equivalents, an info view with a statistic, a checksum calculator, a filter tool
  and a string extraction tool.
  All modifications to the data loaded can be endlessly undone or redone.
 </p>

 <p>
  Additionally to the program there is a module OktetaPart (a KPart) which can be
  configured to be embedded in other programs to view/edit files of certain types.
  Konqueror and KDevelop are examples for programs which are capable of this.
 </p>

 <p>
  Interesting for developers is yet another component named KBytesEdit. This component implements the
  <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/interfaces/khexedit/html/index.html">KHexEdit
  interfaces</a> of the KDE Platform.
 </p>

<!--  <h1>Credits</h1> -->
<?php
// //   kde_general_news("./news.rdf", 10, true);
//   $aboutData = new AboutData( "Okteta" );
//   $aboutData->setIcon( "../../kde-icons/ox32-app-okteta.png", "32", "32" );
//   $aboutData->setVersion( "0.1.0" );
//   $aboutData->setCopyright( "2006", "Friedrich W. H. Kossebau" );
//   $aboutData->setLicense("gpl");
// 
//   $aboutData->addMaintainer("Friedrich W. H. Kossebau", "kossebau@kde.org",
//                      "Current maintainer");
// 
// //   $aboutData->show();
//
?>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

