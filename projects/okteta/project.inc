<?php
class VersionedOktetaGuiPoFileNames
{
    var $poFileNamesArray;

    function VersionedOktetaGuiPoFileNames()
    {
        $beforeKastenSplitPoFileNames = array("okteta","oktetapart","liboktetacore");
        $afterKastenSplitPoFileNames = array("okteta","oktetapart","liboktetacore","libkasten","liboktetakasten");
        $this->poFileNamesArray["4.1"] = $beforeKastenSplitPoFileNames;
        $this->poFileNamesArray["4.2"] = $beforeKastenSplitPoFileNames;
        $this->poFileNamesArray["4.3"] = $beforeKastenSplitPoFileNames;
        $this->poFileNamesArray["4.4"] = $afterKastenSplitPoFileNames;
        $this->poFileNamesArray["4.5"] = $afterKastenSplitPoFileNames;
        $this->poFileNamesArray["4.6"] = $afterKastenSplitPoFileNames;
    }

    function arrayFor($versionId)
    {
        return $this->poFileNamesArray[$versionId];
    }
}

class VersionedOktetaDocPoFileNames
{
    var $poFileNamesArray;

    function VersionedOktetaDocPoFileNames()
    {
        $beforeJoinPoFileNames = array("okteta","okteta_basics","okteta_credits","okteta_introduction","okteta_menu","okteta_tools");
        $afterJoinPoFileNames = array("okteta");
        $this->poFileNamesArray["4.1"] = $beforeJoinPoFileNames;
        $this->poFileNamesArray["4.2"] = $beforeJoinPoFileNames;
        $this->poFileNamesArray["4.3"] = $beforeJoinPoFileNames;
        $this->poFileNamesArray["4.4"] = $afterJoinPoFileNames;
        $this->poFileNamesArray["4.5"] = $afterJoinPoFileNames;
        $this->poFileNamesArray["4.6"] = $afterJoinPoFileNames;
    }

    function arrayFor($versionId)
    {
        return $this->poFileNamesArray[$versionId];
    }
}

    $project = new Project("okteta","Okteta",array(/*"4.6"=>"0.6",*/"4.5"=>"0.5","4.4"=>"0.4","4.3"=>"0.3","4.2"=>"0.2","4.1"=>"0.1"));
    $project->setGuiPoFileNameList( new VersionedOktetaGuiPoFileNames() );
    $project->setDocPoFileNameList( new VersionedOktetaDocPoFileNames() );
?>
