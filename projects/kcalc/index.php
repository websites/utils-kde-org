<?php
    $site_root = "../";
    $page_title = 'KCalc';

    include ( "header.inc" );

    include( "project.inc" );

    $appinfo = new AppInfo( "KCalc" );
    $appinfo->setIcon( "../../kde-icons/hi32-app-kcalc.png", "32", "32" );
    $appinfo->setVersion( "2.2" );
    $appinfo->setCopyright( "1996", "Bernd Johannes Wuebben, Klaus Niederkr&uuml;ger, The KDE Team" );
    $appinfo->setLicense("gpl");

// TODO: the copyright is more complex than AppInfo allows
//       "(c) 2000-2008, The KDE Team\n"
//       "(c) 2003-2005, Klaus Niederkr" "\xc3\xbc" "ger\n"
//       "(c) 1996-2000, Bernd Johannes Wuebben"));

    $appinfo->addAuthor("David Johnson", "david@usermode.org", "Maintainer");
    $appinfo->addAuthor("Klaus Niederkr&uuml;ger", "kniederk@math.uni-koeln.de");
    $appinfo->addAuthor("Bernd Johannes Wuebben", "wuebben@kde.org");
    $appinfo->addContributor("Evan Teran", "eteran@alum.rit.edu");
    $appinfo->addContributor("Espen Sand", "espen@kde.org");
    $appinfo->addContributor("Chris Howells", "howells@kde.org");
    $appinfo->addContributor("Aaron J. Seigo", "aseigo@olympusproject.org");
    $appinfo->addContributor("Charles Samuels", "charles@altair.dhs.org");
    $appinfo->addContributor("René Mérou", "ochominutosdearco@yahoo.es");
    $appinfo->addContributor("Michel Marti", "mma@objectxp.com");

//     $appinfo->show();
?>

 <p>
  KCalc is a calculator which offers many more mathematical functions than meet the eye
  on a first glance. Please study the section on keyboard accelerators and
  modes in the handbook to learn more about the many functions
  available.
 </p>
 <p>
<?php
    $latestRelease = new LatestRelease($project);
    $latestRelease->show();
?>
 </p>

 <div style="text-align: center;">
  <img src="images/kcalc-2.3-screenie.jpg" alt="KCalc Screenshot" />
 </div>

 <p>
  In addition to the usual functionality offered by most scientific
  calculators, KCalc offers a number of features, which I think are
  worthwhile pointing out:
  <ul>
   <li>
    <p>KCalc provides trigonometric functions, logic operations, and it is
    able to do statistical calculations.</p>
   </li>
   <li>
    <p>KCalc allows you to cut and paste numbers from/into its display.</p>
   </li>
   <li>
    <p>KCalc features a <em>results-stack</em> which lets
    you conveniently recall previous results.</p>
   </li>
   <li>
    <p>You can configure KCalc's display colors and font.</p>
   </li>
   <li>
    <p>You can configure KCalc's  precision and the number
    of digits after the period.</p>
   </li>
   <li>
    <p> KCalc offers a great number of useful key-bindings,
    which make using KCalc without using a pointing device easy.</p>
    <p>Hint: pressing (and holding) the Ctrl-key, displays on
    every button, the corresponding key-binding.</p>
   </li>
  </ul>
 </p>


 <?php
//   kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

