<?php
    $vers = upstreamVersionsSince("15.08");
    $vers["15.04"] = "2.13"; // there was no version bump for 15.04
    $vers["14.12"] = "2.13"; // there was no version bump for 14.12
    // there was no version bump for 4.1[1-4]
    for ($i = 14; $i >= 11; $i--)
        $vers["4." . $i] = "2.13";
    $vers["4.10"] = "2.12"; // version bump for 4.10.1
    for ($i = 9; $i >= 0; $i--)
        $vers["4." . $i] = "2." . ($i + 2);

    $vers["3.5"] = "2.0";

    $project = new Project("kcalc","KCalc",$vers);
    $project->setDocPoFileNameList(new VersionedArrayOfArray(array("kcalc","kcalc_commands")));
?>
