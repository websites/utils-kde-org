<?php
    $site_root = "../";
    $page_title = 'KCalc - Development';

    include( "header.inc" );

    include( "project.inc" );

    $developmentOverview = new DevelopmentOverview($project);
//     $blogs = new BlogLinks();
//     $blogs->addBlogLink( "Joe", "http://joe.wordpress.com/tag/kcalc" );
//     $developmentOverview->setBlogs( $blogs );

    $developmentOverview->show();

    include( "footer.inc" );
?>


