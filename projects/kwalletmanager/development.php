<?php
    $site_root = "../";
    $page_title = 'KDE Wallet Manager - Development';

    include( "header.inc" );

    include( "project.inc" );

    $developmentOverview = new DevelopmentOverview($project);
    $developmentOverview->setBugsKdeOrgProductName("kwalletmanager");
//     $blogs = new BlogLinks();
//     $blogs->addBlogLink( "Joe", "http://joe.wordpress.com/tag/kwallet" );
//     $developmentOverview->setBlogs( $blogs );
    $docs = new DocLinks();
    $docs->addDocLink("TechBase information", "http://techbase.kde.org/Projects/Utils/kwallet",
        "The developers' page in the KDE TechBase.");
    $developmentOverview->setDocs( $docs );
    $developmentOverview->show();

    include( "footer.inc" );
?>


