<?php
class KWalletVersionedPath extends VersionedPath
{
    function branchName($versionId)
    {
        if ($versionId=="master")
            return $versionId;

        return "KDE/" . $versionId;
    }
}

    $project = new Project("kwallet","KDE Wallet Manager",array("4.9"=>"1.10","4.8"=>"1.9","4.7"=>"1.8","4.6"=>"1.7","4.5"=>"1.6","4.4"=>"1.5","4.3"=>"1.4","4.2"=>"1.3","4.1"=>"1.2","4.0"=>"1.1","3.5"=>"1.1"));
    $project->setGuiPoFileNameList(new VersionedArrayOfArray(array("kwalletmanager","kcmkwallet")));
    $project->setOtherSourcesList(array("Library"=>new KWalletVersionedPath("kde/kdelibs/repository/show/kdeui/util","kde/kdelibs/repository/show/kdeui/")));
    $project->setOtherAPIList(
        array("Library (KWallet)"=>new VersionedPath("kdelibs-apidocs/kdeui/html/classKWallet_1_1Wallet.html")));
?>
