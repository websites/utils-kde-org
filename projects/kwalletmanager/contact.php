<?php
    $site_root = "../";
    $page_title = 'KDE Wallet Manager - Contact';

    include( "header.inc" );

    include( "project.inc" );

    $contactOverview = new ContactOverview($project);
    $contactOverview->setBugsKdeOrgProductName("kwalletmanager");

    $contactOverview->show();

    include( "footer.inc" );
?>
