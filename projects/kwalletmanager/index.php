<?php
    $site_root = "../";
    $page_title = 'KDE Wallet Manager';

    include ( "header.inc" );

    include( "project.inc" );

    $appinfo = new AppInfo( "KDE Wallet Manager" );
    $appinfo->setIcon( "../../kde-icons/ox32-app-kwalletmanager.png", "32", "32" );
    $appinfo->setVersion( "1.2" );
    $appinfo->setCopyright( "2003", "George Staikos" );
    $appinfo->setLicense("gpl");

    $appinfo->addAuthor("Michael Leupold", "lemma@confuego.org", "Current maintainer");
    $appinfo->addAuthor("George Staikos", "staikos@kde.org", "Author and former maintainer");
    $appinfo->addAuthor("Isaac Clerencia", "isaac@warp.es", "Developer");

//     $appinfo->show();
?>

 <p>
  KDE Wallet Manager is a tool to manage the passwords on your KDE system. By using the
  KDE wallet subsystem it not only allows you to keep your own secrets but also to access
  and manage the passwords of every application that integrates with the KDE wallet.
 </p>

 <p>
<?php
    $latestRelease = new LatestRelease($project);
    $latestRelease->show();
?>
 </p>

 <div style="text-align: center;">
  <img src="images/kwalletmanager-1.2-screenie.jpg" alt="KDE Wallet Manager Screenshot" />
 </div>

 <p>
<!-- some more description -->
 </p>


 <?php
//   kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

