<?php
  $site_root = "../";
  $page_title = 'KGpg - Building from source code';

  include( "header.inc" );
?>

<h2>General</h2>
 <p>KGpg can easily be built from the source code. This can become neccessary
 if you want to try out a new feature currently under development or if you
 face a problem and want to help fixing it.</p>
 <p>The dependencies seldomly change. Since the kdelibs are in a feature freeze
 since a while building a new version of KGpg on an older version of KDE SC will
 usually work. In case you face a problem in an older version of KGpg and your
 distribution does not offer packages of a recent version, or you do not want to
 upgrade your whole workspace, building KGpg from sources should be your first
 action to try if the problem has not already been addresses.</p>

<h2>Installing the dependencies</h2>
 <p>KGpg needs the development files of Qt, the basic KDE libraries (kdelibs) as
 well as the KDE PIM libraries (kdepimlibs). Since version 2.13 (KDE SC 4.14)
 KGpg also depends on the development headers of GPGme.</p>
 <p>To configure the build environment
 <a href="https://www.cmake.org">CMake</a> is required. Finally you need a C++
 compiler and a tool to drive the build. On most Unix-like systems this will
 usually be <em>make</em>, but you may choose another one (e.&nbsp;g. <a
 href="https://ninja-build.org/">Ninja</a>).</p>
 <p>Finally the <a href="https://gitscm.org/">git</a> tool is needed to check out
 the source code in case you do not want to build from a tarball.</p>
 <p>The different distributions give the required package different names, and
 sometimes they fail to record the dependencies from one package (especially the
 development header package) to another. Also the naming may change between
 different releases of the distribution. For this reason the command to install
 the required packages may look different for you.</p>

<h3>OpenSUSE</h3>
 <p><em>zypper in akonadi-contact-devel cmake git libgpgme-devel kdelibs4support-devel libQt5Test-devel make</em></p>

<h3>Debian</h3>
 <p><em>apt-get install cmake g++ git kdepimlibs5-dev libboost-dev libgpgme11-dev make</em></p>
 <p>Tested on Wheezy</p>

<h2>Checking out the source code</h2>
 <h3>First time</h3>
  <p><em>git clone git://anongit.kde.org/kgpg</em></p>
 <h3>Updating the source code</h3>
  <p><em>cd kgpg<br />git pull</em></p>

<h2>Configuring the build</h2>
 <p><em>mkdir build-kgpg<br />
 cd build-kgpg<br />
 cmake -D KGPG_DEBUG_TRANSACTIONS=On -D CMAKE_BUILD_TYPE=Debug ../kgpg</em></p>
 <p>This will give you a build that has all debugging information available
 (useful in case KGpg itself crashes) as well as printing of status information
 in case KGpg communicates with GnuPG.</p>

 <h3>Building</h3>
 <p>To build just type <em>make</em> and wait for the build to finish.</p>

 <h3>Make resource files available</h3>
 <p>The resource files that configure the menu layouts need to be available in the
 correct version. You need to make them available from your build directory if you
 do not install the new version to a supported location, see
 <a href="https://techbase.kde.org/Projects/Documentation/KDE_(health_table)#Docbook_Health_Table">Techbase</a>.
 </p>

<h2>Useful tips</h2>
 <h3>Shut down the old version</h3>
 <p>In case an older version of KGpg is already running starting the newly built
 one will only tell the older one to raise, so you will actually <em>not</em>
 get the behavior of the new version. Either click <em>Quit</em> from the
 <em>File</em> menu or use the shortcut to trigger this action, usually Ctrl-Q.
 Please note that pressing Alt-F4 or closing the window will <em>not</em> quit
 the old KGpg instance in the default configuration, but will just cause it to
 hide in the system tray.</p>
 <p>You can also tell KGpg to quit from the command line:<br />
 <em>qdbus org.kde.kgpg /MainApplication quit</em>

 <h3>Enable debug printing</h3>
 <p>When you run KGpg from the command line it will print out some useful
 status information. This printing can be controlled using the tool
 <em>kdebugdialog</em>. Enter <em>kgpg</em> in the filter at the top of the
 application window and enable all entries you find. Please note that you
 do not need to enable the one labled <em>kgpgconf</em>, as that is a totally
 unrelated application.</p>
<?php
  include( "footer.inc" );
?>
