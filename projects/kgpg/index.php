<?php
    $site_root = "../";
    $page_title = 'KGpg';

    include ( "header.inc" );

    include( "project.inc" );

    $appinfo = new AppInfo( "KGpg" );
    $appinfo->setIcon( "../../kde-icons/hi32-app-kgpg.png", "32", "32" );
    $appinfo->setVersion( "1.9.2" );
    $appinfo->setCopyright( "2003", "Jean-Baptiste Mardelle" );
    $appinfo->setLicense("gpl");

    $appinfo->addAuthor("Jean-Baptiste Mardelle", "bj@altern.org", "Author and former maintainer");
    $appinfo->addAuthor("Jimmy Gilles", "jimmygilles@gmail.com");
    $appinfo->addAuthor("Rolf Eike Beer", "kde@opensource.sf-tec.de", "Maintainer");

//     $appinfo->show();

?>

 <p>
  KGpg is a simple interface for GnuPG, a powerful encryption utility.
 </p>

 <p>
<?php
    $latestRelease = new LatestRelease($project);
    $latestRelease->show();
?>
 </p>

 <p>
  GnuPG (also known as gpg) is included in most distributions and should be
  installed on your system. You can get the latest version on
  <a href="http://gnupg.org">http://gnupg.org/</a>.
 </p>

 <p>
  With KGpg you will be able to encrypt and decrypt your files and emails,
  allowing much more secure communications. A mini howto on encryption with gpg is
  available on <a href="http://www.gnupg.org/(en)/documentation/howtos.html">gnupg's web site</a>.
 </p>

 <p>
  With KGpg, you don't need to remember gpg's command lines and options.
  Almost everything can be done with a few mouse clicks.
 </p>


 <?php
//   kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

