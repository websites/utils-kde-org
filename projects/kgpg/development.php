<?php
    $site_root = "../";
    $page_title = 'KGpg - Development';

    include( "header.inc" );

    include( "project.inc" );

    $developmentOverview = new DevelopmentOverview($project);
//     $blogs = new BlogLinks();
//     $blogs->addBlogLink( "Joe", "http://joe.wordpress.com/tag/kgpg" );
//     $developmentOverview->setBlogs( $blogs );
    $developmentOverview->show();

?>

<h2>Building</h2>
 <p>
 <ul>
   <li>There is a dedicated page collecting <a href="building.php">instructions on how to build KGpg from the source code</a>.</li>
  </ul>
 </p>

<?php
    include( "footer.inc" );
?>


