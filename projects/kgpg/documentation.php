<?php
    $site_root = "../";
    $page_title = 'KGpg - Documentation';

    include( "header.inc" );

    include( "project.inc" );

    $documentationOverview = new DocumentationOverview($project);
    $documentationOverview->show();

    include( "footer.inc" );
?>
