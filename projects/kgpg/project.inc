<?php
     $vers = upstreamVersionsSince("16.12");

     $vers['16.08'] = '2.18';
     $vers['16.04'] = '2.17'; // application bug, I forgot to bump the version number
     // end at 2 as the first version covered here is 14.12
     for ($i = 5; $i >= 2; $i--) {
       $vstring = floor(14 + $i / 3) . '.' . sprintf("%02u", (($i % 3) * 4) + 4);
       $vers[$vstring] = '2.' . (12 + $i);
     }

     for ($i = 14; $i > 0; $i--)
       $vers["4." . $i] = "2." . ($i - 1);

     $vers["4.0"] = "1.7";
     $vers["3.5"] = "1.2";

     $project = new Project("kgpg", "KGpg", $vers);
?>
