<?php
    $site_root = "../";
    $page_title = 'KDiskFree - Development';

    include( "header.inc" );

    include( "project.inc" );

    $developmentOverview = new DevelopmentOverview($project);
//     $blogs = new BlogLinks();
//     $blogs->addBlogLink( "Joe", "http://joe.wordpress.com/tag/kdf" );
//     $developmentOverview->setBlogs( $blogs );
    $developmentOverview->show();

    include( "footer.inc" );
?>
