<?php
    $vers = upstreamVersionsSince("16.08");

    $vers = array_merge($vers, set_version_for_range(14, 12, 16, 4, '0.15'));

    for ($i = 14; $i >= 9; $i--)
      $vers["4." . $i] = '0.15';

     for ($i = 8; $i >= 3; $i--)
       $vers["4." . $i] = '0.' . ($i + 6);

     for ($i = 2; $i >= 0; $i--)
       $vers["4." . $i] = '0.' . ($i + 5);

    $vers["3.5"] = '0.5';

    $project = new Project('kdf', 'KDiskFree', $vers);
?>
