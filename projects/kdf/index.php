<?php
    $site_root = "../";
    $page_title = 'KDiskFree';

    include ( "header.inc" );

    include( "project.inc" );

    $appinfo = new AppInfo( "KDiskFree" );
    $appinfo->setIcon( "../../kde-icons/hi32-app-kdf.png", "32", "32" );
    $appinfo->setVersion( "0.5" );
    $appinfo->setCopyright( "1998", "Michael Kropfberger" );
    $appinfo->setLicense("gpl");

    $appinfo->addAuthor("Michael Kropfberger", "michael.kropfberger@gmx.net");

//     $appinfo->show();
?>

 <p>
  KDiskFree displays the available file devices (hard drive
  partitions, floppy and CD/DVD drives, etc.) along with information on
  their capacity, free space, type and mount point. It also allows you
  to mount and unmount drives and view them in a file manager.
 </p>

 <p>
<?php
    $latestRelease = new LatestRelease($project);
    $latestRelease->show();
?>
 </p>

 <p>
  KDiskFree is similar to the Systemsettings Partitions module,
  but takes up less screen space. It is useful if you want to keep
  a KDiskFree window available at all times.
 </p>


 <?php
//   kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

