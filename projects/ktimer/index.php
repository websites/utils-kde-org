<?php
    $site_root = "../";
    $page_title = 'KTimer';

    include ( "header.inc" );

    include( "project.inc" );

    $appinfo = new AppInfo( "KTimer" );
    $appinfo->setIcon( "../../kde-icons/hi32-app-ktimer.png", "32", "32" );
    $appinfo->setVersion( "0.1.0" );
    $appinfo->setCopyright( "2001", "Stefan Schimanski" );
    $appinfo->setLicense("gpl");

    $appinfo->addAuthor("Stefan Schimanski", "schimmi@kde.org");

//     $appinfo->show();
?>

 <p>
  KTimer is a little tool to execute programs after some time.
 </p>

 <p>
<?php
    $latestRelease = new LatestRelease($project);
    $latestRelease->show();
?>
 </p>

 <p>
<!-- some more description -->
 </p>


 <?php
//   kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

