<?php
    $vers = upstreamVersionsSince("18.12");

    // version 0.10 was used from KDE 4.9 to Applications 18.08
    $vers = array_merge($vers, set_version_for_range(14, 12, 18, 8, '0.10'));
    for ($i = 14; $i >= 9; $i--)
      $vers["4." . $i] = '0.10';

    for ($i = 8; $i >= 0; $i--)
      $vers["4." . $i] = '0.' . ($i + 1);

    $vers["3.5"] = "0.1";

    $project = new Project("ktimer", "KTimer", $vers);
    $project->setDocPoFileNameList(null);
    $project->setManualList(null);
?>
