<?php
    $site_root = "../";
    $page_title = 'KTimer - Contact';

    include( "header.inc" );

    include( "project.inc" );

    $contactOverview = new ContactOverview($project);
    $contactOverview->show();

    include( "footer.inc" );
?>


