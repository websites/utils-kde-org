<?php
    $site_root = "../";
    $page_title = 'KTimer - Development';

    include( "header.inc" );

    include( "project.inc" );

    $developmentOverview = new DevelopmentOverview($project);
//     $blogs = new BlogLinks();
//     $blogs->addBlogLink( "Joe", "http://joe.wordpress.com/tag/ktimer" );
//     $developmentOverview->setBlogs( $blogs );
    $developmentOverview->show();

    include( "footer.inc" );
?>


