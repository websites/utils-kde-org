<?php
    $project = new Project("kremotecontrol","KRemoteControl",array("4.9"=>"4.9","4.8"=>"4.8","4.7"=>"4.7","4.6"=>"4.6","4.5"=>"4.5"));

    $kcmManual =    new Manual("Control module","kcontrol/kremotecontrol");
    $project->setManualList(new VersionedArrayOfArray(array($kcmManual)));
    $project->setDocPoFileNameList(new VersionedArrayOfArray(array("kcontrol_kremotecontrol")));
    $project->setGuiPoFileNameList(new VersionedArrayOfArray(array("libkremotecontrol","kcm_kremotecontrol","kremotecontroldaemon","krcdnotifieritem")));
?>
