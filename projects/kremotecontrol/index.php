<?php
    $site_root = "../";
    $page_title = 'KRemoteControl';

    include ( "header.inc" );

    include( "project.inc" );
?>

  <p>
    KRemoteControl (formerly known as <a href="http://utils.kde.org/projects/kdelirc">KDELirc</a>) is a KDE frontend
    for your remote controls. It allows to configure actions for button presses on remotes.
    All types of remotes supported by the <a href="http://solid.kde.org/">Solid module</a> in the KDE platform
    are also supported by KRemoteControl
    (e.g. with the <a href="http://lirc.org/">Linux Infrared Remote Control system (LIRC)</a> as backend).
  </p>

  <p>
<?php
    $latestRelease = new LatestRelease($project);
    $latestRelease->show();
?>
  </p>

<!--
  <div style="text-align: center;">
    <img src="images/kdelirc-4.3-screenie.jpg" alt="Screenshot of IRKick and the KDELirc control module" />
  </div>
-->
  <p>
    KRemoteControl consists of the following parts:
    <ul>
      <li>
        libkremotecontrol:<br>
        This library holds all the basic data structures for handling remotes,
        modes and actions. Additionally it holds a D-Bus browser interface and executors for actions.
      </li>
      <li>
        kcmremotecontrol:<br>
        This is the configuration module for systemsettings. It allows to create and assign actions to remote controls.
      </li>
      <li>
        kded:<br>
        A kded module responsible for actually executing the actions on incoming button presses.
      </li>
      <li>
        krcdnotifieritem:<br>
        A notifier item sitting in the notification area (aka system tray).
        Its job is to notify the user of incoming button presses by blinking
        and to provide a context menu for manually switching a remotes mode.
      </li>
      <li>
        Plasma dataengine:<br>
        It allows to interact with the kded module within a plasmoid. There is no plasmoid provided with KRemoteControl yet.
      </li>
    </ul>

 <?php
//   kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

