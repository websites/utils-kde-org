<?php
    $site_root = "./";
    $page_title = "KDE Utilities";

    include "header.inc";

    $latestStableKdeVersionId = "?";
    foreach($kdeVersionStateTags as $versionId => $versionState)
    {
        if ($versionState == "R")
        {
            $latestStableKdeVersionId = $versionId;
            break;
        }
    }
?>

 <p>
  The KDE Utilities are a compilation of various desktop utilities,
  developed in the KDE module <b>kdeutils</b>.
 </p>

 <p>
  The latest stable release is included in <b><?php echo $kdeVersionNameList[$latestStableKdeVersionId]." ".$latestStableKdeVersionId ?></b>.
  The KDE Utilities are free and open source software, available for Linux and similar operating systems.
 </p>

 <div style="text-align: center;">
  <img src="images/utils-4.1-screenie.jpg" alt="KDE Utilities Screenshot" />
 </div>


<?php
    $teaser = new ProjectTeaser("Files");
    $teaser->addProject("ark","Ark","Handle file archives.",
                        "kde-icons/ox32-app-utilities-file-archiver.png","32","32");
    $teaser->addProject("kbackup","KBackup","Backup your files.",
                        "kde-icons/32-apps-kbackup.png","32","32");
    $teaser->addProject("kdf","KDiskFree","View free disk space.",
                        "kde-icons/ox32-app-kdf.png","32","32");
    $teaser->addProject("filelight","Filelight","Understand usage of disk space.",
                        "kde-icons/hi32-app-filelight.png","32","32");
    $teaser->show();

    $teaser = new ProjectTeaser("Security");
    $teaser->addProject("kgpg","KGpg","Control your GPG keys.",
                        "kde-icons/ox32-app-kgpg.png","32","32");
    $teaser->addProject("kwalletmanager","KDE Wallet Manager","Manage your passwords.",
                        "kde-icons/ox32-app-kwalletmanager.png","32","32");
    $teaser->addProject("sweeper","Sweeper","Clean unwanted traces from your system.",
                        "kde-icons/ox32-actions-trash-empty.png","32","32");
    $teaser->show();

    $teaser = new ProjectTeaser("Hardware");
    $teaser->addProject("kfloppy","KFloppy","Format floppy disks.",
                        "kde-icons/hi32-app-kfloppy.png","32","32");
    $teaser->show();

    $teaser = new ProjectTeaser("Various");
    $teaser->addProject("kcalc","KCalc","Do scientific calculations.",
                        "kde-icons/ox32-app-accessories-calculator.png","32","32");
    $teaser->addProject("kcharselect","KCharSelect","Select special characters from any font.",
                        "kde-icons/ox32-app-accessories-character-map.png","32","32");
    $teaser->addProject("ktimer","KTimer","Execute programs after some time.",
                        "kde-icons/hi32-app-ktimer.png","32","32");
    $teaser->show();

    include "footer.inc";
?>
