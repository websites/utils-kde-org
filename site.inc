<?php
$templatepath = "newlayout/";
$site_title = "KDE Utilities";
$site_external = true;
$site = 'utils';
$name = "utils.kde.org Webmaster";
$mail = 'kde@opensource.sf-tec.de';
$showedit = false;
// version of the upcoming release
$latest_version = 22.08;

// trunk first, oldest last
$kdeVersionNameList = array();
$kdeActiveVersions = array();

// add all versions from $latest_version down to 14.12
for ($i = $latest_version; $i >= 14.12; $i -= 0.04) {
  // switch from e.g. 15.00 to 14.12
  if (floor($i) == floor($i + 0.99)) {
    $i -= (1.00 - 0.12);
  }
  $vstring = strval($i);
  array_push($kdeActiveVersions, $vstring);
  $kdeVersionNameList[$vstring] = "KDE Applications";
}

array_push($kdeActiveVersions, "4.14");
$kdeVersionNameList["4.14"] = "KDE Applications";

for ($i = 13; $i >= 0; $i--) {
  $vstring = "4." . $i;
  array_push($kdeActiveVersions, $vstring);
  $kdeVersionNameList[$vstring] = $i >= 4 ? "KDE SC" : "KDE";
}
array_push($kdeActiveVersions, "3.5");
$kdeVersionNameList["3.5"] = "KDE";

$kdeVersionStateTags = array(
 $kdeActiveVersions[0] => 'D'
);

foreach(array_slice($kdeActiveVersions, 1) as $ver) {
  $kdeVersionStateTags[$ver] = "R";
}

$versionStateTexts = array(
  "D" => "Development",
  // support ALPHA, Beta, RC?
  "P" => "To be released",
  "R" => "Released"
);
$kdeTrunkVersionId = $kdeActiveVersions[0];
$kdeOldestActiveVersionId = $kdeActiveVersions[count($kdeActiveVersions)-1];

function upstreamVersionsSince($start)
{
  $vers = array();
  global $kdeActiveVersions;

  foreach($kdeActiveVersions as $version) {
    if($version < $start)
      break;
    $vers[$version] = $version;
  }

  return $vers;
}

/**
 * @brief set the same application version for a range of KDE Applications releases
 *
 * The lowest supported version is 14.12, just because it was the first
 * KDE applications release.
 */
function set_version_for_range($start_major, $start_minor, $end_major, $end_minor, $app_version)
{
  $vers = array();

  for ($i = ($end_major - 14) * 3 + ($end_minor / 4) - 1; $i >= ($start_major - 14) * 3 + ($start_minor / 4) - 1; $i--) {
    $vstring = floor(14 + $i / 3) . '.' . sprintf("%02u", (($i % 3) * 4) + 4);
    $vers[$vstring] = $app_version;
  }

  return $vers;
}

$piwikSiteID = 31;
$piwikEnabled = true;

// TODO: L10n

include( "includes/class_projectteaser.inc" );
include( "includes/class_license.inc" );
include( "includes/class_latestrelease.inc" );
include( "includes/class_versionedpath.inc" );
include( "includes/class_versionedarrayofarray.inc" );
include( "includes/class_project.inc" );
include( "includes/class_contactoverview.inc" );
include( "includes/class_documentationoverview.inc" );
include( "includes/class_developmentoverview.inc" );
include( "includes/class_apidoxlinks.inc" );
include( "includes/class_bloglinks.inc" );
include( "includes/class_doclinks.inc" );
include( "includes/class_bugskdeorglinks.inc" );
include( "includes/class_ebnreportlinks.inc" );
include( "includes/class_l10nlinks.inc" );
include( "includes/class_manual.inc" );
include( "includes/class_manuallinks.inc" );
include( "includes/class_userbaselinks.inc" );
include( "includes/class_repoactivitylinks.inc" );
include( "includes/class_websvnlinks.inc" );
?>
