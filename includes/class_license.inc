<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class License
{
    var $name;
    var $link;

    function License($licenseId)
    {
        if( $licenseId == "gpl" )
        {
            $this->name = "GNU General Public License (GPL), Version 2";
            $this->link = "http://www.gnu.org/licenses/gpl.html";
        }
        else if( $licenseId == "lgpl" )
        {
            $this->name = "GNU Lesser General Public License (LGPL), Version 2";
            $this->link = "http://www.gnu.org/licenses/lgpl.html";
        }
        else
        {
            $this->name = "Unknown license";
        }
    }
}

?>
