<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class DocLinks
{
    var $items = array();
    var $blog;

    function DocLinks()
    {
    }

    function addDocLink($name, $url, $description)
    {
        $doclink = new DocLink($name, $url, $description);
        array_push($this->items, $doclink);
    }

    function show()
    {
        print
"
 <p>
  <img src=\"../../kde-icons/ox32-document-multiple.png\" align=\"right\" hspace=\"20\" />
  Browse the documents of the developers:
 </p>\n";

        print "<ul>\n";
        foreach($this->items as $doclink)
        {
            print "<li>";
            $doclink->show();
            print "</li>\n";
        }
        print "</ul>\n";
    }
}

class DocLink
{
    var $name;
    var $url;
    var $description;

    function DocLink($name, $url, $description=null)
    {
        $this->name = $name;
        $this->url = $url;
        $this->description = $description;
    }

    function show()
    {
        print "<a href=\"$this->url\">$this->name</a>";
        if($this->description)
            print " - ".$this->description;
    }
}

?>
