<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class EBNReportLinks
{
    var $project;
    var $docsSanitizerShown = true;

    function EBNReportLinks($project)
    {
        $this->project = $project;
    }

    function show()
    {
        $kde4_xDocVersionId = "4.2";

        print
"
 <p>\n
  See the result of the checks for ".$this->project->name." by <a href=\"http://ebn.kde.org/\">EBN</a>:\n
 </p>\n
 <ul>\n
  <li><a href=\"http://ebn.kde.org/krazy/reports/kde-4.x/kdeutils/".$this->project->id."/index.html\">Code Checking</a></li>\n
  <li><a href=\"http://ebn.kde.org/apidocs/apidox-kde-4.x/kdeutils-".$this->project->id.".html\">API Documentation</a></li>\n
";
        $versionedDocPoFileNameList = $this->project->versionedDocPoFileNameList;
/* Handbook Sanitizer currently unavailable
        if($versionedDocPoFileNameList && $versionedDocPoFileNameList->arrayFor($kde4_xDocVersionId))
        {
            print
"
  <li><a href=\"http://ebn.kde.org/sanitizer/reports/kde-4.x/kdeutils/".$this->project->id."/index.html\">Handbook Sanitizer</a></li>\n
";
        }
*/
        print
"
 </ul>\n
";
    }
}

?>
