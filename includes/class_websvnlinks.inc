<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class WebSvnLinks
{
    var $project;

    function WebSvnLinks($project)
    {
        $this->project = $project;
    }

    function show()
    {
        global $kdeActiveVersions;
        global $kdeVersionNameList;
        global $kdeVersionStateTags;
        global $versionStateTexts;

        $kdeVersionPathList = array(
            $kdeActiveVersions[0] => "master"
        );

        foreach(array_slice($kdeActiveVersions, 1) as $ver) {
            if($ver >= "14.12")
                $kdeVersionPathList[$ver] = 'Applications/' . $ver;
            else
                $kdeVersionPathList[$ver] = 'KDE/' . $ver;
        }

        print
"
 <p>\n
  <img src=\"../../kde-icons/ox32-c++src.png\" align=\"right\" hspace=\"20\" />
  Browse the sources of " . $this->project->name . " in the <a href=\"https://invent.kde.org/\">KDE Source Repository</a> online:\n
 </p>\n

 <table>\n
  <tr>\n
   <th>Version</th><th>Sources</th>\n
  </tr>\n";

        $projectId = $this->project->id;
        $versions = $this->project->versions;
        $otherSourcesList = $this->project->otherSourcesList;

        foreach($kdeActiveVersions as $kdeVersionId)
        {
            if (array_key_exists($kdeVersionId,$kdeVersionPathList)
                && array_key_exists($kdeVersionId,$versions))
            {
                $versionId = $versions[$kdeVersionId];
                $versionPath = $kdeVersionPathList[$kdeVersionId];
                $versionStateText = $versionStateTexts[$kdeVersionStateTags[$kdeVersionId]];

                print
"
  <tr>\n
   <td><b>" . $versionId . '</b> (' . $kdeVersionNameList[$kdeVersionId] . ' ' . $kdeVersionId . ', ' . $versionStateText . "):</td>\n
   <td><a href=\"https://invent.kde.org/utilities/" . $projectId . "/-/tree/v" . $kdeVersionId . "\">Program</a>";

//                 foreach ($otherSourcesList as $name => $path)
//                     print
// ",
//    <a href=\"https://invent.kde.org/projects/".$path->path($kdeVersionId)."?rev=".$path->branchName($versionPath)."\">".$name."</a>";

                print
"  </td>\n
  </tr>\n";
            }
        }

        print
"
 </table>\n
";
    }
}

?>
