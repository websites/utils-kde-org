<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class DevelopmentOverview
{
    // TODO: could that name be delivered by some project.inc file?
    var $project;
    var $bugsKdeOrgProductName;
    var $blogs;
    var $docs;

    /**
    * @param $projectId text id of the project in the kdeutils module (dir name)
    * @param $projectName Name of the Project
    */
    function DevelopmentOverview($project)
    {
        global $kdeOldestActiveVersionId;

        $this->project = $project;
        $this->bugsKdeOrgProductName = $project->id;
    }

    /**
     * default is projectId, as defined by the constructor parameter
     */
    function setBugsKdeOrgProductName($bugsKdeOrgProductName)
    {
        $this->bugsKdeOrgProductName = $bugsKdeOrgProductName;
    }
    function setBlogs($blogs)
    {
        $this->blogs = $blogs;
    }
    function setDocs($docs)
    {
        $this->docs = $docs;
    }

    function show()
    {
        print "<h2><a name=\"progress\">Follow Progress</a></h2>\n";

        $repoactivity = new RepoActivityLinks($this->project);
        $repoactivity->show();

        if ($this->blogs)
            $this->blogs->show();

        if ($this->docs)
            $this->docs->show();

        print "<h2><a name=\"tasks\">Select a Task</a></h2>";

        $ebn = new EBNReportLinks($this->project);
        $ebn->show();

        $bko = new BugsKdeOrgLinks($this->bugsKdeOrgProductName);
        $bko->show();

        $l10n = new L10nLinks($this->project);
        $l10n->show();

        print "<h2><a name=\"overview\">Get an Overview</a></h2>";

        $websvn = new WebSvnLinks($this->project);
        $websvn->show();

        $apidox = new ApiDoxLinks($this->project);
        $apidox->show();
    }
}

?>
