<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class ApiDoxLinks
{
    var $project;

    function ApiDoxLinks($project)
    {
        $this->project = $project;
    }

    function show()
    {
        global $kdeActiveVersions;
        global $kdeVersionNameList;
        global $kdeVersionStateTags;
        global $versionStateTexts;

        $kdeVersionPathList = array(
            $kdeActiveVersions[0] => "4.x-api/",
            '4.14' => '4.14-api/'
        );

        print
"
 <p>\n
  <img src=\"../../kde-icons/ox32-c++hdr.png\" align=\"right\" hspace=\"20\" />
  Understand the architecture of ".$this->project->name."
  and see for classes and methods by looking
  in the <a href=\"http://api.kde.org/\">KDE API Reference</a> for ".$this->project->name.":\n
 </p>\n

 <table>\n
  <tr>\n
   <th>Version</th><th>API Reference</th>\n
  </tr>\n";

        $projectId = $this->project->id;
        $versions = $this->project->versions;
        $otherAPIList = $this->project->otherAPIList;

        foreach($kdeActiveVersions as $kdeVersionId)
        {
            if (array_key_exists($kdeVersionId,$kdeVersionPathList)
                && array_key_exists($kdeVersionId,$versions))
            {
                $versionId = $versions[$kdeVersionId];
                $versionPath = $kdeVersionPathList[$kdeVersionId];
                $versionStateText = $versionStateTexts[$kdeVersionStateTags[$kdeVersionId]];

                print
"
  <tr>\n
   <td><b>" . $versionId . '</b> (' . $kdeVersionNameList[$kdeVersionId] . ' ' . $kdeVersionId . ', ' . $versionStateText . "):</td>\n
   <td><a href=\"http://api.kde.org/".$versionPath."kdeutils-apidocs/".$projectId."/html/index.html\">Program</a>";

                foreach ($otherAPIList as $name => $path)
                    print
",
   <a href=\"http://api.kde.org/".$versionPath.$path->path($kdeVersionId)."\">".$name."</a>";

                print
"  </td>\n
  </tr>\n";
            }
        }

        print
"
 </table>\n
";
    }
}

?>
