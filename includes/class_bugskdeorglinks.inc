<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class BugsKdeOrgLinks
{
    var $productname;

    function BugsKdeOrgLinks($productname)
    {
        $this->productname = $productname;
    }

    function show()
    {
        // TODO: make the icon use css
        // TODO: make the icon use an absolute url, not relative, will break
        // also care for the other icons
        print
"
 <p>\n
  <img src=\"../../kde-icons/ox32-bug.png\" align=\"right\" hspace=\"20\" />
  Look what could be worked on next by scanning the <a href=\"http://bugs.kde.org/\">KDE Bug Tracking System</a>:\n
 </p>\n
 <ul>\n
  <li>
  <a href=\"http://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&amp;short_desc=&amp;product=".$this->productname."&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;bug_severity=critical&amp;bug_severity=grave&amp;bug_severity=major&amp;bug_severity=crash\">Major Bug reports</a>\n
  </li>\n
  <li>
  <a href=\"http://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&amp;short_desc=&amp;product=".$this->productname."&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;bug_severity=normal&amp;bug_severity=minor\">Minor Bug reports</a>\n
  </li>\n
  <li>
  <a href=\"http://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&amp;short_desc=&amp;product=".$this->productname."&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;bug_severity=wishlist\">Wish reports</a>\n
  </li>\n
 </ul>\n
";
    }
}

?>
