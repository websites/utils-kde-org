<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class RepoActivityLinks
{
    var $project;

    function RepoActivityLinks($project)
    {
        $this->project = $project;
    }

    function show()
    {
        print
"
 <p>\n
  <img src=\"../../kde-icons/ox32-vcs-commit.png\" align=\"right\" hspace=\"20\" />
  Watch how development of ".$this->project->name." is going on
  by visiting <a href=\"https://invent.kde.org/utilities/" . $this->project->id . ".git/\"
  >the project activity page</a>.
 </p>\n
";
    }
}

?>
