<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class LatestRelease
{
    var $name;
    var $version;
    var $kdeVersion;
    var $stateTag;
    var $licenseId;

    function LatestRelease($project)
    {
        global $kdeActiveVersions;
        global $kdeVersionStateTags;

        $this->name = $project->name;
        $this->version = "";
        $this->kdeVersion = "";
        $this->stateTag = "";
        $this->licenseId = $project->licenseId;

        // find latest release or, if none found, most upcoming one
        // assumes D < P < R for given releases
        foreach($project->versions as $kdeVersionId => $versionId)
        {
            if (array_key_exists($kdeVersionId,$kdeVersionStateTags))
            {
                $kdeVersionStateTag = $kdeVersionStateTags[$kdeVersionId];
                $this->version = $versionId;
                $this->kdeVersion = $kdeVersionId;
                $this->stateTag = $kdeVersionStateTag;
                if ($kdeVersionStateTag == "R")
                    break;
            }
        }
    }

    function show()
    {
        global $kdeVersionNameList;

        // not any version noted yet for an existing/planned KDE package version
        if( $this->version == "" )
            return;

        $license = new License($this->licenseId);

        if( $this->stateTag == "R" )
            print
"
  The latest stable release is version <b>".$this->version."</b>,
  included in ".$kdeVersionNameList[$this->kdeVersion]." ".$this->kdeVersion.".\n
";
        else
            print
"
  The first stable release is going to be version <b>".$this->version."</b>,
  included in ".$kdeVersionNameList[$this->kdeVersion]." ".$this->kdeVersion.".\n
";

        print
$this->name." is a free and open source software, available for Linux and similar operating systems under the\n
  <a href=\"".$license->link."\">".$license->name."</a>.\n
";
    }
}

?>
