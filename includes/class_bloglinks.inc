<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class BlogLinks
{
    var $items = array();
    var $blog;

    function BlogLinks()
    {
    }

    function addBlogLink($name, $url)
    {
        $blog = new BlogLink($name, $url);
        array_push($this->items, $blog);
    }

    function show()
    {
        print "
 <p>
  <img src=\"../../kde-icons/ox32-blog.png\" align=\"right\" hspace=\"20\" />
  Read the blogs of the developers:
 </p>\n";

        print "<ul>\n";
        foreach($this->items as $bloglink)
        {
            print "<li>";
            $bloglink->show();
            print "</li>\n";
        }
        print "</ul>\n";
    }
}

class BlogLink
{
        var $name;
        var $url;

        function BlogLink($name, $url)
        {
                $this->name = $name;
                $this->url = $url;
        }

        function show()
        {
                print "<a href=\"$this->url\">$this->name</a>";
        }
}

?>
