<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class ContactOverview
{
    // TODO: could that name be delivered by some project.inc file?
    var $project;
    var $bugsKdeOrgProductName;

    /**
    * @param $projectId text id of the project in the kdeutils module (dir name)
    * @param $projectName Name of the Project
    */
    function ContactOverview($project)
    {
        $this->project = $project;
        $this->bugsKdeOrgProductName = $project->id;
    }

    /**
     * default is projectId, as defined by the constructor parameter
     */
    function setBugsKdeOrgProductName($bugsKdeOrgProductName)
    {
        $this->bugsKdeOrgProductName = $bugsKdeOrgProductName;
    }

    function show()
    {
        $name = $this->project->name;

        print "
<h2><a name=\"bugs\">Report bugs</a></h2>\n
 <p>\n
  <img src=\"../../kde-icons/ox32-bug.png\" align=\"right\" hspace=\"20\" />
  If you encounter bugs or would like to propose features, please use the\n
  <a href=\"https://bugs.kde.org/wizard.cgi\">Bug Reporting Wizard</a> at\n
  <a href=\"https://bugs.kde.org/\">KDE Bug Tracking System</a>.\n
  Your report should be filed for the product <b>".$this->bugsKdeOrgProductName."</b>.\n
 </p>\n

<h2><a name=\"users\">Discuss with other users</a></h2>\n
 <p>\n
  <img src=\"../../kde-icons/ox32-users.png\" align=\"right\" hspace=\"20\" />
  For discussions about the usage of ".$name.", subscribe to the\n
  <a href=\"http://mail.kde.org/mailman/listinfo/kde\">general KDE mailinglist</a> for users,\n
  if your needs were not already covered by browsing it's\n
  <a href=\"http://lists.kde.org/?l=kde&amp;r=1&amp;w=2\">online archive</a>.\n
 </p>\n
 <p>\n
  Alternatively use the web-based <a href=\"http://forum.kde.org/\">KDE forum</a>.\n
 </p>\n
 <p>\n
  Or chat on one of the <a href=\"http://userbase.kde.org/IRC_Channels\">KDE-related IRC channels</a>
  in the <a href=\"http://www.freenode.net/\">freenode</a> IRC Network,
  e.g. channel <a href=\"irc://irc.kde.org/#kde\">#kde</a>.\n
 </p>\n

<h2><a name=\"developers\">Talk to developers</a></h2>\n
 <p>\n
  <img src=\"../../kde-icons/ox32-users.png\" align=\"right\" hspace=\"20\" />
  For discussions around the development of ".$name.", subscribe to the\n
  <a href=\"http://mail.kde.org/mailman/listinfo/kde-utils-devel\">KDE Utilities developer mailinglist</a>,\n
  and try it's <a href=\"http://lists.kde.org/?l=kde-utils-devel&amp;r=1&amp;w=2\">online archive</a>.\n
  Or chat on the <a href=\"http://userbase.kde.org/IRC_Channels\">IRC channel</a>
  in the <a href=\"http://www.freenode.net/\">freenode</a> IRC Network for KDE Utilities development,
  <a href=\"irc://irc.kde.org/#kde-utils\">#kde-utils</a>.\n
 </p>\n
 <p>\n
  If you want to provide a patch, please file a review request on the <a href=\"http://reviewboard.kde.org/\">KDE Review Board</a> for the group <b>kdeutils</b>.\n
 </p>\n";

// <h2><a name=\"feedback\">Give Feedback</a></h2>
//  <p>
//   For general questions and feedback you can also send an email to the author
//   <a href=\"mailto:kossebau@kde.org\">Friedrich W. H. Kossebau</a> directly.
//  </p>
    }
}

?>
