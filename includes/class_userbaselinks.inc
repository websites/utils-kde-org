<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class UserBaseLinks
{
    // TODO: could that name be delivered by some project.inc file?
    var $project;

    /**
    * @param $project data of the project
    */
    function UserBaseLinks($project)
    {
        $this->project = $project;
    }

    function show()
    {
        $wikiStyleProjectName = str_replace( ' ', '_', $this->project->name );
        print
"
 <p>\n
  <img src=\"../../kde-icons/ox32-document-multiple.png\" align=\"right\" hspace=\"20\" />
  Read and write usage information related to ".$this->project->name." at the
  <a href=\"http://userbase.kde.org/\">KDE UserBase</a>:\n
 </p>\n
 <ul>\n
  <li>\n
  <a href=\"http://userbase.kde.org/".$wikiStyleProjectName."\">Main page</a>\n
  </li>\n
 </ul>\n
";
    }
}

?>
