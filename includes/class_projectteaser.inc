<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class ProjectTeaser
{
    var $name;
    var $items = array(0=>array());
    var $wraps = 0;

    function ProjectTeaser($name)
    {
        $this->name = $name;
    }

    function addProject($id,$name,$description,$iconPath=null,$iconWidth=null,$iconHeight=null,$remoteHomepage=null)
    {
        $data = new ProjectTeaseData($id,$name,$description,$iconPath,$iconWidth,$iconHeight,$remoteHomepage);
        array_push($this->items[$this->wraps], $data);
    }

    function addWrap()
    {
        ++$this->wraps;
        $this->items[$this->wraps] = array();
    }

    function show()
    {
        print "<h3>$this->name</h3>\n";
        foreach($this->items as $items)
        {
            print "<table><tr>\n";
            foreach($items as $item)
            {
                print "<td valign=\"top\" width=\"130\">";
                $item->show();
                print "</td>\n";
            }
            print "</tr></table>\n";
        }
    }
}

class ProjectTeaseData
{
    var $id;
    var $name;
    var $description;
    var $iconPath;
    var $iconWidth;
    var $iconHeight;
    var $remoteHomepage;

    function ProjectTeaseData($id,$name,$description,$iconPath=null,$iconWidth=null,$iconHeight=null,$remoteHomepage=null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->iconPath = $iconPath;
        $this->iconWidth = $iconWidth;
        $this->iconHeight = $iconHeight;
        $this->remoteHomepage = $remoteHomepage;
    }

    function show()
    {
        if( $this->remoteHomepage )
            $url = $this->remoteHomepage;
        else
            $url = "projects/".$this->id;

        print "
 <table>\n
  <tr>\n";
        if( $this->iconPath )
            print "
   <td valign=\"top\"><a href=\"$url\"><img src=\"$this->iconPath\"  alt=\"$this->name\" width=\"$this->iconWidth\" height=\"$this->iconHeight\" /></a></td>\n";
        print "
   <td><a href=\"$url\"><b>$this->name</b></a><br />\n
    $this->description<br />\n
    <a href=\"$url\">More...</a></td>\n
  </tr>\n
 </table>\n";
    }
}

?>
