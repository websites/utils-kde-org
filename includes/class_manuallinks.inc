<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class ManualLinks
{
    var $project;

    function ManualLinks($project)
    {
        $this->project = $project;
    }

    function show()
    {
        global $kdeActiveVersions;
        global $kdeVersionNameList;
        global $kdeVersionStateTags;
        global $versionStateTexts;

        $handbookVersionPathList = array(
            $kdeActiveVersions[0] => "development",
            $kdeActiveVersions[1] => "stable");

        $versionedManualList = $this->project->versionedManualList;

        print
"
 <p>\n
  <img src=\"../../kde-icons/ox32-manual.png\" align=\"right\" hspace=\"20\" />
  Consult the handbook of ".$this->project->name." online
  in the <a href=\"http://docs.kde.org/\">KDE Documentation</a>:\n
 </p>\n

 <table>\n
  <tr>\n
   <th>Version</th><th></th>\n
  </tr>\n";
        $projectId = $this->project->id;
        $versions = $this->project->versions;

        foreach($kdeActiveVersions as $kdeVersionId)
        {
            if (array_key_exists($kdeVersionId,$handbookVersionPathList)
                && array_key_exists($kdeVersionId,$versions)
                && $versionedManualList->arrayFor($kdeVersionId))
            {
                $versionId = $versions[$kdeVersionId];
                $versionPath = $handbookVersionPathList[$kdeVersionId];
                $versionStateText = $versionStateTexts[$kdeVersionStateTags[$kdeVersionId]];
                $manualList = $versionedManualList->arrayFor($kdeVersionId);

                print
"
  <tr>\n
   <td><b>" . $versionId . '</b> (' . $kdeVersionNameList[$kdeVersionId] . ' ' . $kdeVersionId . ', ' . $versionStateText . "):</td>\n
   <td>";


                    $first = true;
                    foreach( $manualList as $manual)
                    {
                        if ($first)
                            $first = false;
                        else
                            print ", ";
                        print "<a href=\"http://docs.kde.org/".$versionPath."/en/kdeutils/".$manual->path."\">".$manual->name."</a>";
                    }
                print
"</td>\n
  </tr>\n";
            }
        }

        print
"
 </table>\n
";
    }
}

?>
