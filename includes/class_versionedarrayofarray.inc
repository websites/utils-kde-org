<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class VersionedArrayOfArray
{
    var $normalArray;

    function VersionedArrayOfArray($normalArray)
    {
        $this->normalArray = $normalArray;
    }

    /**
     * @return path for versionId
     */
    function arrayFor($versionId)
    {
        return $this->normalArray;
    }
}

?>
