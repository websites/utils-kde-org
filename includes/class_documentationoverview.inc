<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class DocumentationOverview
{
    // TODO: could that name be delivered by some project.inc file?
    var $project;

    /**
    * @param $project data of the project
    */
    function DocumentationOverview($project)
    {
        $this->project = $project;
    }

    function show()
    {
        if ($this->project->versionedManualList)
        {
            print "<h2><a name=\"manual\">Read the User Manual</a></h2>\n";

            $manual = new ManualLinks($this->project);
            $manual->show();
        }

        print "<h2><a name=\"userbase\">Share the Knowledge</a></h2>";

        $userBaseLinks = new UserBaseLinks($this->project);
        $userBaseLinks->show();
    }
}

?>
