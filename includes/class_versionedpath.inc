<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class VersionedPath
{
    var $path;

    function VersionedPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return path for versionId
     */
    function path($versionId)
    {
        return $this->path;
    }

    /**
     * @return branch name, like "KDE/4.6" or "4.6"
     */
    function branchName($versionId)
    {
        return $versionId;
    }
}

?>
