<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class L10nLinks
{
    var $project;

    function L10nLinks($project)
    {
        $this->project = $project;
    }

    function show()
    {
        global $kdeActiveVersions;
        global $kdeVersionNameList;
        global $kdeVersionStateTags;
        global $versionStateTexts;

        $translationVersionPathList = array(
            $kdeActiveVersions[0] => "trunk-kf5",
            $kdeActiveVersions[1] => "stable-kf5");

        print "
 <p>\n
  <img src=\"../../kde-icons/ox32-locale.png\" align=\"right\" hspace=\"20\" />
  Find out if you could help to adapt ".$this->project->name." to your locale
  in the <a href=\"http://l10n.kde.org/\">KDE Localization</a>:\n
 </p>\n

 <table>\n
  <tr>\n
   <th>Version</th><th>GUI Translation</th><th>Handbook Translation</th>\n
  </tr>\n";

        $versions = $this->project->versions;
        $projectId = $this->project->id;
        $versionedGuiPoFileNameList = $this->project->versionedGuiPoFileNameList;
        $versionedDocPoFileNameList = $this->project->versionedDocPoFileNameList;

        $guiPoFileNameList = null;
        $docPoFileNameList = null;

        foreach($kdeActiveVersions as $kdeVersionId)
        {
            if (array_key_exists($kdeVersionId,$translationVersionPathList)
                && array_key_exists($kdeVersionId,$versions))
            {
                $versionId = $versions[$kdeVersionId];
                $versionPath = $translationVersionPathList[$kdeVersionId];
                $versionStateText = $versionStateTexts[$kdeVersionStateTags[$kdeVersionId]];

                if ($versionedGuiPoFileNameList)
                    $guiPoFileNameList = $versionedGuiPoFileNameList->arrayFor($kdeVersionId);
                if ($versionedDocPoFileNameList)
                    $docPoFileNameList = $versionedDocPoFileNameList->arrayFor($kdeVersionId);

                if (!$guiPoFileNameList && !$docPoFileNameList)
                    continue;

                print "
  <tr>\n
   <td><b>" . $versionId . '</b> (' . $kdeVersionNameList[$kdeVersionId] . ' ' . $kdeVersionId . ', ' . $versionStateText . "):</td>\n
   <td>";

                if ($guiPoFileNameList)
                {
                    $first = true;
                    foreach( $guiPoFileNameList as $poFileName)
                    {
                        if ($first)
                            $first = false;
                        else
                            print ",";
                        print "
  <a href=\"http://l10n.kde.org/stats/gui/".$versionPath."/po/".$poFileName.".po/\">".$poFileName.".po</a>";
                    }
                }
                else
                    print "-";

                print "
   </td>\n
   <td>";
                if ($docPoFileNameList)
                {
                    $first = true;
                    foreach( $docPoFileNameList as $poFileName)
                    {
                        if ($first)
                            $first = false;
                        else
                            print ", ";
                        print "
   <a href=\"http://l10n.kde.org/stats/doc/".$versionPath."/po/".$poFileName.".po/\">".$poFileName.".po</a>";
                    }
                }
                else
                    print "-";

                print "
   </td>\n
  </tr>\n";
            }
        }

        print "
 </table>\n";
    }
}

?>
