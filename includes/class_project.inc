<?php

/**
 * Written by Friedrich W. H. Kossebau <kossebau@kde.org>
 */

class Project
{
    // TODO: read this from an xml file
    var $name;
    var $id;
    var $versions;
    var $versionedManualList;
    var $versionedGuiPoFileNameList;
    var $versionedDocPoFileNameList;
    var $otherSourcesList;
    var $otherAPIList;
    var $licenseId;

    /**
    * @param $id text id of the project in the kdeutils module (dir name)
    * @param $name Name of the Project
    * @param $versions array which maps project version to KDE version
    */
    function Project($id,$name,$versions=null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->versions = $versions;
        $this->versionedManualList = new VersionedArrayOfArray(array(new Manual($name,$id)));
        $this->versionedGuiPoFileNameList = new VersionedArrayOfArray(array($id));
        $this->versionedDocPoFileNameList = new VersionedArrayOfArray(array($id));
        $this->otherSourcesList = array();
        $this->otherAPIList = array();
        $this->licenseId = "gpl";
    }

    /**
     * default is VersionedArrayOfArray(new Manual($name,$id))
     */
    function setManualList($versionedManualList)
    {
        $this->versionedManualList = $versionedManualList;
    }

    /**
     * default is VersionedArrayOfArray(array($id))
     */
    function setGuiPoFileNameList($versionedGuiPoFileNameList)
    {
        $this->versionedGuiPoFileNameList = $versionedGuiPoFileNameList;
    }
    /**
     * default is VersionedArrayOfArray(array($id))
     */
    function setDocPoFileNameList($versionedDocPoFileNameList)
    {
        $this->versionedDocPoFileNameList = $versionedDocPoFileNameList;
    }
    /**
     * if there are sources in other KDE modules, add here.
     * array is build from entries name => path where path is 
     * default is empty array.
     */
    function setOtherSourcesList($otherSourcesList)
    {
        $this->otherSourcesList = $otherSourcesList;
    }
    /**
     * if there are sources in other KDE modules, add here.
     * array is build from entries name => path where path is 
     * default is empty array.
     */
    function setOtherAPIList($otherAPIList)
    {
        $this->otherAPIList = $otherAPIList;
    }
}

?>
